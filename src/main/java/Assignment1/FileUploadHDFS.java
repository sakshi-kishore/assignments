package Assignment1;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.io.*;
import java.net.URI;
public class FileUploadHDFS {
    public static void main(String args[]) throws IOException {
        String localPath = "/Users/sakshi/IdeaProjects/Capstone_Assignments/src/main/resources/Assignment-1-Input_Files";
        String uri = "hdfs://localhost:9000";
        String hdfsDir = "hdfs://localhost:9000/User/Files";
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(uri), conf);
        for (int i = 0; i < 100; i++) {
            String FilePath = localPath + "/File_" + i + ".csv";
            fs.copyFromLocalFile(new Path(FilePath), new Path(hdfsDir));
        }
    }
}
