To run this program, first install hadoop and hbase in your System.

Following steps has been used to create this assignment.

1. Added hadoop dependencies inside build.graddle file.
2. Created required 100 csv files and stored them inside resources folder. (Path - /src/main/resources/Assignment-1-Input_Files/).
3. Created directory in hdfs using command - (hadoop fs -mkdir /User, hadoop fs -mkdir /User/Files) in command prompt.
4. Created Java program 'FileUploadHDFS.java' to upload 100 csv files in hdfs(Path - /User/Files).
5. Created Java program 'HdFStoHBase.java' to create table(People_Info') in hbase and upload these csv data from hdfs to hbase table.